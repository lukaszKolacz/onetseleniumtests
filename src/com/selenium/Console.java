package com.selenium;

public class Console {

//    private int temperature = 0;

    public void MethodIf() {
        int temperature = 0;
        if (temperature > 0) {
            System.out.println("It's cold: " + temperature);
        } else if (temperature == 0) {
            System.out.println("It's zero: " + temperature);
        } else {
            System.out.println("It's warm: " + temperature);
        }
    }

    public void MethodWhile() {
        int temperature = 0;
        while (temperature < 15) {
            System.out.println("Turn the heat up: " + temperature);
            temperature++;
        }
        System.out.println("Now it's fine: " + temperature);
    }

    public void MethodDoWhile() {
        int temperature = 0;

        do {
            System.out.println("Turn the heat up: " + temperature);
            temperature++;
        }
        while (temperature <= 15);
    }

    public void MethodFor() {
        int temperature = 0;
        for (int i = 0; i < 5; i++) {
            temperature = i;
            System.out.println("It's rising: " + temperature);
        }
    }

    public void MethodSwitch() {
        int temperature = 2;
        switch (temperature) {
            case -1:
                System.out.println("Case 1");
                break;
            case 0:
                System.out.println("Case 2");
                break;
            case 1:
                System.out.println("Case 3");
                break;
            default:
                System.out.println("Case default");
                break;
        }
    }
}
