package com.selenium;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.chrome.*;

public class Google {

    @Test
    public void MethodGoogle() throws InterruptedException {

        System.setProperty("webdriver.chrome.driver", "C:\\Users\\Tester\\Downloads\\chromedriver_win32\\chromedriver.exe");
        ChromeDriver driver = new ChromeDriver();

        driver.get("https://www.google.pl/");
        driver.findElement(By.name("q")).sendKeys("Sii");
//        driver.findElement(By.name("q")).click();
//        By click enter key
        driver.findElement(By.name("q")).sendKeys(Keys.ENTER);

        Thread.sleep(1000);
        String locator = "//*[@id=\"rso\"]/div[1]/div/div[1]/div/div/div[1]/a/h3";
//        String badLocator = "//*[@id=\"rso\"]/div[1]/div/div[4]/div/div/div[1]/a/h3";

        String text = "Sii Polska: Rozwiązania i usługi IT i inżynierii_bug";
        String actual = driver.findElement(By.xpath(locator)).getText();

        Assert.assertEquals("NotWorks", text, actual);


    }
}
