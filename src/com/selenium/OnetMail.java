package com.selenium;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.*;
import org.openqa.selenium.support.ui.*;


public class OnetMail {
    public void OnetMailMaethod() throws InterruptedException {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\Tester\\Downloads\\chromedriver_win32\\chromedriver.exe");

        ChromeDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://www.onet.pl/poczta?state=https://poczta.onet.pl/&app_id=poczta.onet.pl.front.onetapi.pl");

//        Thread.sleep(1000); - the same method like Wait but wors.

        WaitByXpath(driver, "//*[text()='Przejdź do serwisu']");

        driver.findElement(By.xpath("//*[text()='Przejdź do serwisu']")).click();
        driver.findElement(By.id("mailFormLogin")).sendKeys("testerselenium@onet.pl");
        driver.findElement(By.id("mailFormPassword")).sendKeys("tester.selenium1");
        driver.findElement(By.id("mailFormPassword")).sendKeys(Keys.ENTER);

        WaitByXpath(driver, "//*[text()='Napisz wiadomość']");

        driver.findElement(By.xpath("//*[text()='Napisz wiadomość']")).click();

//        WaitById(driver,"field-recipient-to");
//        Thread.sleep(2000);

        driver.findElement(By.xpath("//*[@id=\"field-recipient-to\"]/div/div/div[1]/input")).sendKeys("lukaszkolacz15@gmail.com");
        Thread.sleep(2000);

        driver.findElement(By.xpath("//*[@id=\"field-subject\"]/div/div/input")).sendKeys("Selenium Example Title");
        Thread.sleep(2000);

//        //*[@id="wrapper"]/div[3]/div/div[1]/section/div[2]/div[5]/div/div[2]/div/textarea
        driver.findElement(By.xpath("//*[@id=\"CONTENT_TEXTAREA_ID_ifr\"]")).sendKeys("Lorem ipsum");


//        driver.quit();
    }

    public static void WaitByXpath(WebDriver driver2, String path) {
        WebDriverWait wait = new WebDriverWait(driver2, 20);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(path)));
    }

    public static void WaitById(WebDriver driver2, String element_id) {
        WebDriverWait wait = new WebDriverWait(driver2, 20);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id(element_id)));
    }
}
